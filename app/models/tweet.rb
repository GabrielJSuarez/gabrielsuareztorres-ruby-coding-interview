class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { in: 1..180 }
  validates :has_been_posted?

  def has_been_posted?
    tweet = Tweet.find_by(body: self.body, created_at: (0.hours.ago..24.hours.ago))
    errors.add.(:created_at, "You can't create the same tweet in less than 24 hours") if tweet
  end

end
